# schoolBbs

#### 介绍
九职校园论坛app  
此项目为前端Android项目，[后台项目](https://gitee.com/lightning23333/bbs_servlet)

#### 软件架构
分为论坛模块、聊天模块和用户信息模块


#### 使用说明

1.  在我的界面注册账号
2.  可以浏览论坛和发布论坛
3.  在论坛内可以评论点赞
4.  在聊天界面在已知账号的情况下进行实时聊天

#### 实现效果

![](https://cdn.jsdelivr.net/gh/moyvch/myImage@master/202201012250635.jpg)
![](https://cdn.jsdelivr.net/gh/moyvch/myImage@master/202201012251223.jpg)
![](https://cdn.jsdelivr.net/gh/moyvch/myImage@master/202201012251728.jpg)
![](https://cdn.jsdelivr.net/gh/moyvch/myImage@master/202201012251097.jpg)


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


