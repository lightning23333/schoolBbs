package com.moyv.schoolbbs;

import com.moyv.schoolbbs.util.PhotoUtil;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;


import java.io.File;
import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.moyv.schoolbbs.config.URLConstant.MAIN_URL;
import static com.moyv.schoolbbs.util.HttpUtil.createClient;
import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }
    @Test
    public void test(){
        PhotoUtil photoUtil=new PhotoUtil();
        byte[] bytes=photoUtil.loadImage(new File("C:\\Users\\放手去闯\\Pictures\\Saved Pictures\\飞雪PC端.jpg"));
        String byteToString=photoUtil.byteToString(bytes);
        String compress=photoUtil.compress(byteToString);
        System.out.println(compress);
    }
    @Test
    public void test1(){
        String url=MAIN_URL;
        OkHttpClient okHttpClient=createClient();
//        请求体
        FormBody formBody=new FormBody.Builder()
                .add("account","5554")
                .build();
//        发送请求操作
        Request request=new Request.Builder()
                .post(formBody)
                .url(url)
                .build();
        okHttpClient.newCall(request).enqueue(new Callback(){
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                System.out.println("mainService:获取main的响应对象失败");


            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                System.out.println("mainService:拿到main的响应对象");
                String responseData=response.body().string();
                try {
                    JSONObject objMain=new JSONObject(responseData);
                    System.out.println(objMain.getJSONObject("main"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        System.out.println("hello");
    }
}