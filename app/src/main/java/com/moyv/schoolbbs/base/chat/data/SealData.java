package com.moyv.schoolbbs.base.chat.data;

import android.database.Cursor;

import com.moyv.schoolbbs.bean.model.Message;
import com.moyv.schoolbbs.bean.model.User;
import com.moyv.schoolbbs.config.URLConstant;

import java.util.ArrayList;
import java.util.Date;

public class SealData {

    public static ArrayList<User> getUsers(String users) {
        ArrayList<User> userArrayList = new ArrayList<>();
        String[] user = users.split(";");
        for (String s :
                user) {
            userArrayList.add(getUser(s));
        }
        return userArrayList;
    }

//    public static User getUser(String name) {
//        return new User(name + "@" + URLConstant.C_DOMAIN, name, URLConstant.HEAD_URL + name + ".jpg", false);
//    }

    public static User getUser(String account) {
        return new User(account, null, getPhoto(account), false);
    }

    public static String getPhoto(String account) {
        return URLConstant.HEAD_URL + account + ".jpg";
    }

    public static String getId(String account) {
        return account + "@" + URLConstant.C_DOMAIN;
    }

    public static Message getMessage(String account, String mes, Date date) {
        return new Message(account, getUser(account), mes, date);
    }

    public static String getCursorString(Cursor cursor,String name){
        return cursor.getString(cursor.getColumnIndex(name));
    }
}
