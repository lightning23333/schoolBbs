package com.moyv.schoolbbs.base.chat.data;

import android.app.Service;
import android.content.Context;
import android.database.Cursor;

import com.moyv.schoolbbs.bean.model.Dialog;
import com.moyv.schoolbbs.bean.model.Message;
import com.moyv.schoolbbs.bean.model.User;
import com.moyv.schoolbbs.config.URLConstant;
import com.moyv.schoolbbs.database.DatabaseUtil;
import com.moyv.schoolbbs.ui.service.ChatService;

import java.util.ArrayList;
import java.util.Date;

import static com.moyv.schoolbbs.base.chat.data.SealData.getCursorString;
import static com.moyv.schoolbbs.base.chat.data.SealData.getId;
import static com.moyv.schoolbbs.base.chat.data.SealData.getMessage;
import static com.moyv.schoolbbs.base.chat.data.SealData.getPhoto;
import static com.moyv.schoolbbs.base.chat.data.SealData.getUser;
import static com.moyv.schoolbbs.base.chat.data.SealData.getUsers;

public class DialogData {
    private String name;
    private DatabaseUtil databaseUtil;

    public DialogData(Context context, String name) {
        this.name = name;
        this.databaseUtil = new DatabaseUtil(context, name);

    }

    public String getName() {
        return name;
    }

    public Dialog CreateDialog(String account, String n, String mes) {
        Message message = getMessage(account, mes, new Date());
        ArrayList<User> users = getUsers(account);
        saveDialog(account, n, account, mes);
        return new Dialog(account, n, getPhoto(account), users, message, 1);
    }

    public ArrayList<Dialog> getLocalRecord() {
        ArrayList<Dialog> chats = new ArrayList<>();
        Cursor cursor = databaseUtil.getAll("Conversation");
        if (cursor != null && cursor.moveToFirst()) {
            do {
                String n = cursor.getString(cursor.getColumnIndex("name"));//账号
                Message message = getLastMessage(n);
                ArrayList<User> users = getUsers(cursor.getString(cursor.getColumnIndex("users")));
                String Name = cursor.getString(cursor.getColumnIndex("title"));
                Dialog dialog = new Dialog(n, Name, getPhoto(n), users, message, cursor.getShort(cursor.getColumnIndex("unreadCount")));
                chats.add(dialog);
            } while (cursor.moveToNext());
        }
        return chats;
    }

    public Message getLastMessage(String id) {
        Cursor cursor = databaseUtil.getMessageLast(id);
        if (cursor.moveToFirst())
            return getMessage(getCursorString(cursor, "username"), getCursorString(cursor, "mes"), new Date(getCursorString(cursor, "time")));
        return null;
    }

    public ArrayList<Message> getLocalMessages(String id) {
        ArrayList<Message> messages = new ArrayList<>();
        Cursor cursor = databaseUtil.getMessageALL(id);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                Message message = getMessage(getCursorString(cursor, "username"), getCursorString(cursor, "mes"), new Date(getCursorString(cursor, "time")));
                messages.add(message);
            } while (cursor.moveToNext());
        }
        return messages;
    }

    public ArrayList<Message> getLocalMessages(String id, int from, int len) {
        ArrayList<Message> messages = new ArrayList<>();
        Cursor cursor = databaseUtil.getMessageLimit(id, String.valueOf(from), String.valueOf(len));
        if (cursor != null && cursor.moveToFirst()) {
            do {
                Message message = getMessage(getCursorString(cursor, "username"), getCursorString(cursor, "mes"), new Date(getCursorString(cursor, "time")));
                messages.add(message);
            } while (cursor.moveToNext());
        }
        return messages;
    }

    public void saveMessage(Message message) {
        saveMessage(message.getId(), message.getUser().getId(), message.getText(), message.getCreatedAt(), true);
    }

    public void saveMessage(String account, String username, String mes, boolean isChat) {
        databaseUtil.insertMessage(account, username, mes, new Date(), isChat);
    }

    public void saveMessage(String account, String username, String mes, Date date, boolean isChat) {
        databaseUtil.insertMessage(account, username, mes, date, isChat);
    }

    public void saveDialog(String account, String title, String users, String unreadCount) {
        databaseUtil.insertDialog(account, title, users, unreadCount);
    }

    public void updateDialogUnreadCount(String account, int count) {
        databaseUtil.updateDialog(account, String.valueOf(count));
    }
}
