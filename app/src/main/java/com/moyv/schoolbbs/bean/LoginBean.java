package com.moyv.schoolbbs.bean;

import com.google.gson.annotations.SerializedName;

/**
 * @author moyv
 */
public class LoginBean {
    @SerializedName("name")
    private String name;
    @SerializedName("account")
    private Integer account;
    @SerializedName("introduction")
    private String introduction;
    @SerializedName("status")
    private Integer status;
    @SerializedName("token")
    private String token;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAccount() {
        return account;
    }

    public void setAccount(Integer account) {
        this.account = account;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
