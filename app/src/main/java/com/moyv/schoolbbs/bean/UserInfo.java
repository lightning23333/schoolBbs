package com.moyv.schoolbbs.bean;

import com.google.gson.annotations.SerializedName;

/**
 * @author moyv
 * @描述 用户信息封装
 */
public class UserInfo {
    @SerializedName("head")
    private String head;
    @SerializedName("name")
    private String name;
    @SerializedName("account")
    private Integer account;
    @SerializedName("introduction")
    private String introduction;

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAccount() {
        return account;
    }

    public void setAccount(Integer account) {
        this.account = account;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }
}
