package com.moyv.schoolbbs.bean;

import com.google.gson.annotations.SerializedName;
import com.moyv.schoolbbs.pojo.Comment;
import com.moyv.schoolbbs.pojo.Post;
import com.moyv.schoolbbs.pojo.Star;

import java.util.List;

/**
 * @author moyv
 * @描述 首页返回信息封装类
 */
public class ReModel {
    @SerializedName("main")
    private List<Main> main;
    @SerializedName("count")
    private int count;

    public List<Main> getMain() {
        return main;
    }

    public void setMain(List<Main> main) {
        this.main = main;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    /**
     * 一个子布局信息封装
     */
    public static class Main {
        @SerializedName("comments")
        private List<Comment> comments;
        @SerializedName("post")
        private Post post;
        @SerializedName("stars")
        private List<Star> stars;
        @SerializedName("userInfo")
        private UserInfo userInfo;
        @SerializedName("photos")
        private List<String> photos;

        public List<String> getPhotos() {
            return photos;
        }

        public void setPhotos(List<String> photos) {
            this.photos = photos;
        }

        public UserInfo getUserInfo() {
            return userInfo;
        }

        public void setUserInfo(UserInfo userInfo) {
            this.userInfo = userInfo;
        }

        public List<Comment> getComments() {
            return comments;
        }

        public void setComments(List<Comment> comments) {
            this.comments = comments;
        }

        public Post getPost() {
            return post;
        }

        public void setPost(Post post) {
            this.post = post;
        }

        public List<Star> getStars() {
            return stars;
        }

        public void setStars(List<Star> stars) {
            this.stars = stars;
        }


    }
}
