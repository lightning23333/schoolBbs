package com.moyv.schoolbbs.util;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

public class HttpUtil {
    public static OkHttpClient createClient(){
        //        创建okhttpClinet
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10,TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .build();
        return okHttpClient;
    }
}
