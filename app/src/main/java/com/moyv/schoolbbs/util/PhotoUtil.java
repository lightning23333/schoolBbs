package com.moyv.schoolbbs.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.GZIPOutputStream;




public class PhotoUtil {

    public static void merge(String srcFiles, String destFilePath) {
        //判断原文件路径是否为空
        if (srcFiles == null) {
            throw new IllegalArgumentException(srcFiles + " must be not null");
        }
        //创建目标文件夹，并且判断目标文件夹是否存在
        File destFile = new File(destFilePath);
        if (!destFile.exists()) {
            boolean effect = destFile.mkdirs();
            if (!effect) {
                throw new IllegalArgumentException(destFile + " create failed");
            }
        }
        //获取原文件夹中所有的文件
        File[] files = listAllFiles(srcFiles);
        for (File src : files) {
            //如果文件格式不是jpg或者png，就不参与文件合并
            if (!src.getName().endsWith("jpg") && !src.getName().endsWith("png")) {
                continue;
            }
            //记录每个参与合并的文件的字节长度
            int length = (int) src.length();
            //进行文件传入和目的文件的传出
            try (FileInputStream in = new FileInputStream(src);
                 FileOutputStream out = new FileOutputStream(new File(destFile, "mergeP2.jpg"), true)) {
                //将每个参与合并的图片文件的长度加在文件字节前，用于记录参与合并的文件长度，并且在解压文件时起到指示此文件解压完成的作用
                //这里需要将文件的长度转换成byte数组，再传入目的文件中
                //length -> byte[]   82(int) -> 32(bit)
                for (int i = 0; i < trans(length).length; i++) {
//                    System.out.println(trans(length)[i]);
                    out.write(trans(length)[i]);
                }
                //读取原文件中的字节内容
                byte[] buff = new byte[1024];
                int len = -1;
                while ((len = in.read(buff)) != -1) {
                    out.write(buff, 0, len);
                }
                out.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //实现int类型转换为byte数组
    public static byte[] trans(int data) {
        //int转换成byte[] 不需要 & 0xFF
        //0xFF是字面量：00000000 00000000 00000000 11111111
        byte[] ret = new byte[4];
        ret[0] = (byte) (data >> 24);
        ret[1] = (byte) (data >> 16);
        ret[2] = (byte) (data >> 8);
        ret[3] = (byte) data;
        return ret;
    }

    //实现byte数组转换成int类型
    public static int byteArrayToInt(byte[] buff) {
        //byte[] 转换成int 必须 & 0xFF
        int values = 0;
        for (int i = 0; i < 4; i++) {
            int shift = (3 - i) * 8;
            values += (buff[i] & 0xFF) << shift;
        }
        return values;
    }

    //用来显示原文件夹下所有的文件，简化添加图片文件的步骤
    public static File[] listAllFiles(String srcFilePath) {
        File srcFile = new File(srcFilePath);
        if (srcFile.isFile()) {
            return new File[]{srcFile};
        }
        return srcFile.listFiles();
    }

    //测试
    public static void main(String[] args) {
//        String[] srcFiles = {"D:" + File.separator + "TestCode" + File.separator + "iotest" + File.separator + "data-a.txt",
//                "D:" + File.separator + "TestCode" + File.separator + "iotest" + File.separator + "data-b.txt",
//                "D:" + File.separator + "TestCode" + File.separator + "iotest" + File.separator + "data-c.txt"};
//        String destFilePath = "D:" + File.separator + "TestCode" + File.separator + "iotest" + File.separator + "data.txt";
//        merge(srcFiles, destFilePath);

        String srcFiles = "D:" + File.separator + "TestCode" + File.separator + "iotest" + File.separator + "spiltP";
        String destFilePath = "D:" + File.separator + "TestCode" + File.separator + "iotest" + File.separator + "mergePicture";
        long start = System.currentTimeMillis();
        merge(srcFiles, destFilePath);
        long end = System.currentTimeMillis();
        System.out.println(end-start);

    }


    public String photoProcess(File file){
        byte[] bytes=loadImage(file);
        String byteToString=byteToString(bytes);
        String compress=compress(byteToString);
        return compress;
    }

    /**
     * 将图片转换为字节数组
     * @return
     */
    public byte[] loadImage(File file){
        //用于返回的字节数组
        byte[] data=null;
        //打开文件输入流
        FileInputStream fin=null;
        //打开字节输出流
        ByteArrayOutputStream bout=null;
        try{
            //文件输入流获取对应文件
            fin=new FileInputStream(file);
            //输出流定义缓冲区大小
            bout=new ByteArrayOutputStream((int)file.length());
            //定义字节数组，用于读取文件流
            byte[] buffer=new byte[1024];
            //用于表示读取的位置
            int len=-1;
            //开始读取文件
            while((len=fin.read(buffer))!=-1){
                //从buffer的第0位置开始，读取至第len位置，结果写入bout
                bout.write(buffer,0,len);
            }
            //将输出流转为字节数组
            data=bout.toByteArray();
            //关闭输入输出流
            fin.close();
            bout.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        return data;
    }
    /**
     * 把字节数组转化为字符串----"ISO-8859-1"
     * @param
     * @return
     */
    public String byteToString(byte[] data){
        String dataString=null;
        try{
            //将字节数组转为字符串，编码格式为ISO-8859-1
            dataString=new String(data,"ISO-8859-1");
        }catch(Exception e){
            e.printStackTrace();
        }
        return dataString;
    }
    /**
     * 压缩字符串----"ISO-8859-1"
     * @param data
     * @return
     */
    public String compress(String data){
        String finalData=null;
        try{
            //打开字节输出流
            ByteArrayOutputStream bout=new ByteArrayOutputStream();
            //打开压缩用的输出流,压缩后的结果放在bout中
            GZIPOutputStream gout=new GZIPOutputStream(bout);
            //写入待压缩的字节数组
            gout.write(data.getBytes("ISO-8859-1"));
            //完成压缩写入
            gout.finish();
            //关闭输出流
            gout.close();
            finalData=bout.toString("ISO-8859-1");
        }catch(Exception e){
            e.printStackTrace();
        }
        return finalData;
    }

}
