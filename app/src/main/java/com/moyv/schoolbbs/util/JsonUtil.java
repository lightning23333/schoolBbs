package com.moyv.schoolbbs.util;

import com.google.gson.Gson;
import com.moyv.schoolbbs.bean.LoginBean;
import com.moyv.schoolbbs.bean.ReModel;

/**
 * @author moyv
 */
public class JsonUtil {
    public static ReModel parseReModel(String jsonData) {
        ReModel reModel = null;
        try {
            Gson gson = new Gson();
            reModel = gson.fromJson(jsonData, ReModel.class);
            // data就是整个JSON数据映射的一个对象
            return reModel;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("JsonUtil.ReModel:解析返回数据失败");
        }
        return null;

    }
    public static LoginBean parseLoginBean(String jsonData){
        LoginBean loginBean = null;
        try {
            Gson gson = new Gson();
            loginBean = gson.fromJson(jsonData, LoginBean.class);
            // data就是整个JSON数据映射的一个对象
            return loginBean;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("JsonUtil.LoginBean:解析返回数据失败");
        }
        return null;
    }

    /**
     * 解析简单Json数据
     */

    public static <T> T parseJson(String jsonData,Class<T> entityType){
        Gson gson=new Gson();
        T t=gson.fromJson(jsonData,entityType);
        return t;
    }
}
