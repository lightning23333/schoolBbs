package com.moyv.schoolbbs.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * @author moyv
 */
public class AnalysisUtils {
    /**
     * 读取用户名
     */

    public static String readLoginUserName(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences("login",Context.MODE_PRIVATE);
        String userName=sharedPreferences.getString("name","");
        return userName;
    }

    /**
     * 读取登录状态
     */

    public static boolean readLoginStatus(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences("login",Context.MODE_PRIVATE);
        boolean isLogin=sharedPreferences.getBoolean("isLogin",false);
        return isLogin;
    }

    /**
     * 清除登录状态
     */

    public static void cleanLoginStatus(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences("login",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("isLogin",false);
        editor.putString("token","");
//        editor.putString("name","");
//        editor.putInt("account",0);
//        editor.putString("introduction","");
        editor.commit();
    }
}
