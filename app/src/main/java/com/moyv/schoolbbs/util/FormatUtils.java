package com.moyv.schoolbbs.util;

import android.os.Build;

import androidx.annotation.RequiresApi;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/*
 * Created by troy379 on 06.04.17.
 */
public final class FormatUtils {
    public static final long ONE_DAY_MILLIS = 24 * 60 * 60 * 1000;
    public static final long ONE_HOUR_MILLIS = 60 * 60 * 1000;
    public static final long ONE_MINUTE_MILLIS = 60 * 1000;
    private FormatUtils() {
        throw new AssertionError();
    }

    public static String getDurationString(int seconds) {
        Date date = new Date(seconds * 1000);
        SimpleDateFormat formatter = new SimpleDateFormat(seconds >= 3600 ? "HH:mm:ss" : "mm:ss");
        formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
        return formatter.format(date);
    }

    public static String getFormatData(long start, long now){
        String result="0000-00-00";
        long timer=now-start;
        if(timer<ONE_DAY_MILLIS&&timer>ONE_HOUR_MILLIS){
            SimpleDateFormat formatDay = new SimpleDateFormat("H小时前",
                    Locale.getDefault());
            Date date = new Date(timer);
            result=formatDay.format(date);
        }else if(timer<ONE_HOUR_MILLIS&&timer>ONE_MINUTE_MILLIS){
            SimpleDateFormat formatDay = new SimpleDateFormat("m分钟前",
                    Locale.getDefault());
            Date date = new Date(timer);
            result=formatDay.format(date);
        }else if (timer<ONE_MINUTE_MILLIS&&timer>0){
            SimpleDateFormat formatDay = new SimpleDateFormat("s秒前",
                    Locale.getDefault());
            Date date = new Date(timer);
            result=formatDay.format(date);
        }else if(timer>ONE_DAY_MILLIS){
            int date= 0;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                date = Math.toIntExact(timer / ONE_DAY_MILLIS);
            }
            result=date+"天前";
        }
        return result;
    }
}
