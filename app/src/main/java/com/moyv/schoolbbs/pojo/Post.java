package com.moyv.schoolbbs.pojo;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

/**
 * @author moyv
 */
public class Post implements Serializable {
    @SerializedName("postType")
    private Integer postType;
    @SerializedName("postAccount")
    private Integer postAccount;
    @SerializedName("postContent")
    private String postContent;
    @SerializedName("postTitle")
    private String postTitle;
    @SerializedName("postId")
    private Integer postId;
    @SerializedName("proTimer")
    private String proTimer;
    @SerializedName("starNum")
    private Integer starNum;

    public Integer getPostType() {
        return postType;
    }

    public void setPostType(Integer postType) {
        this.postType = postType;
    }

    public Integer getPostAccount() {
        return postAccount;
    }

    public void setPostAccount(Integer postAccount) {
        this.postAccount = postAccount;
    }

    public String getPostContent() {
        return postContent;
    }

    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public Integer getPostId() {
        return postId;
    }

    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    public String getProTimer() {
        return proTimer;
    }

    public void setProTimer(String proTimer) {
        this.proTimer = proTimer;
    }

    public Integer getStarNum() {
        return starNum;
    }

    public void setStarNum(Integer starNum) {
        this.starNum = starNum;
    }

}
