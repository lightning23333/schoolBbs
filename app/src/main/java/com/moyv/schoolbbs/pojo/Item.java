package com.moyv.schoolbbs.pojo;


import com.moyv.schoolbbs.bean.UserInfo;
import com.moyv.schoolbbs.bean.ReModel;

import java.util.List;

/**
 * @author moyv
 */
public class Item {
    private ReModel.Main main;
    private List<String> urls;
    private UserInfo userInfo;

    public ReModel.Main getMain() {
        return main;
    }

    public void setMain(ReModel.Main main) {
        this.main = main;
    }

    public List<String> getImagesUrls() {
        return urls;
    }

    public void setImagesUrls(List<String> urls) {
        this.urls = urls;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }
}
