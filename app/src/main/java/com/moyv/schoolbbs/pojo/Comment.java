package com.moyv.schoolbbs.pojo;

import com.google.gson.annotations.SerializedName;
import com.moyv.schoolbbs.bean.UserInfo;

import java.sql.Timestamp;
import java.util.Date;

/**
 * @author moyv
 * @描述 评论实体类
 */
public class Comment {
    @SerializedName("commentId")
    private int commentId;
    @SerializedName("commentContent")
    private String commentContent;
    @SerializedName("commentAccount")
    private int commentAccount;
    @SerializedName("commentTime")
    private String commentTime;
    @SerializedName("postId")
    private int postId;
    @SerializedName("cmtName")
    private String cmtName;
    // 回复人信息
    private UserInfo replyUser;
    // 评论人信息
    private UserInfo commentsUser;

    public UserInfo getReplyUser() {
        return replyUser;
    }

    public void setReplyUser(UserInfo replyUser) {
        this.replyUser = replyUser;
    }

    public UserInfo getCommentsUser() {
        return commentsUser;
    }

    public void setCommentsUser(UserInfo commentsUser) {
        this.commentsUser = commentsUser;
    }

    public String getCmtName() {
        return cmtName;
    }

    public void setCmtName(String cmtName) {
        this.cmtName = cmtName;
    }

    public int getCommentId() {
        return commentId;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    public String getCommentContent() {
        return commentContent;
    }

    public void setCommentContent(String commentContent) {
        this.commentContent = commentContent;
    }

    public int getCommentAccount() {
        return commentAccount;
    }

    public void setCommentAccount(int commentAccount) {
        this.commentAccount = commentAccount;
    }

    public String getCommentTime() {
        return commentTime;
    }

    public void setCommentTime(String commentTime) {
        this.commentTime = commentTime;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }
}
