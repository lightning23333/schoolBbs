package com.moyv.schoolbbs.pojo;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Star {
    @SerializedName("statId")
    private int statId;
    @SerializedName("postId")
    private int postId;
    @SerializedName("starAccount")
    private int starAccount;
    @SerializedName("starTime")
    private String starTime;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @SerializedName("name")
    private String name;

    public int getStatId() {
        return statId;
    }

    public void setStarId(int statId) {
        this.statId = statId;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public int getStarAccount() {
        return starAccount;
    }

    public void setStarAccount(int starAccount) {
        this.starAccount = starAccount;
    }

    public String getStarTime() {
        return starTime;
    }

    public void setStarTime(String starTime) {
        this.starTime = starTime;
    }
}
