package com.moyv.schoolbbs.pojo;

/**
 * @author moyv
 */
public class HandlerPosition {
    private int position;

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public Object getObj() {
        return obj;
    }

    public void setObj(Object obj) {
        this.obj = obj;
    }

    private Object obj;
}
