package com.moyv.schoolbbs.database;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class MyDatabaseHelper extends SQLiteOpenHelper {

    public MyDatabaseHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        final String ConversationTable="CREATE TABLE Conversation (" +
                "id INTEGER NOT NULL UNIQUE," +
                "name TEXT," +
                "title TEXT,"+
                "users TEXT," +
                "unreadCount INTEGER," +
                "PRIMARY KEY(id AUTOINCREMENT)" +
                ");";
        final String MessageTable="CREATE TABLE Message (" +
                "id INTEGER NOT NULL UNIQUE,"+
                "name TEXT," +//与Conversation的name对应
                "username TEXT," +
                "mes TEXT," +
                "ischat INTEGER DEFAULT 0," +
                "time TEXT," +
                "PRIMARY KEY(id AUTOINCREMENT)"+
                ");";
        db.execSQL(ConversationTable);
        db.execSQL(MessageTable);
        System.out.println("数据库创建成功！！！");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //版本更新时执行
    }
}
