package com.moyv.schoolbbs.service;

import android.os.Handler;
import android.util.Log;

import com.moyv.schoolbbs.pojo.Comment;
import com.moyv.schoolbbs.pojo.HandlerPosition;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static android.content.ContentValues.TAG;
import static com.moyv.schoolbbs.config.AccountConstant.TOKEN;
import static com.moyv.schoolbbs.config.URLConstant.CMT_URL;
import static com.moyv.schoolbbs.util.HttpUtil.createClient;
import static com.moyv.schoolbbs.util.JsonUtil.parseJson;

public class CmtService {
    public void pushCmt(Comment comment, Handler handler, int position) {
        String url = CMT_URL;
        OkHttpClient okHttpClient = createClient();
//        请求体
        JSONObject cmt = new JSONObject();
        try {
            cmt.put("postId", comment.getPostId());
            cmt.put("account", comment.getCommentAccount());
            cmt.put("content", comment.getCommentContent());
        } catch (JSONException e) {
            Log.d(TAG, "pushCmt: json操作有问题");
        }


        FormBody formBody = new FormBody.Builder()
                .add("comment", cmt.toString())
                .add("token", TOKEN)
                .build();
//        发送请求操作
        Request request = new Request.Builder()
                .post(formBody)
                .url(url)
                .build();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                handler.sendMessage(handler.obtainMessage(404));
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                String responseData = response.body().string();
                JSONObject object = null;
                JSONObject objComment = null;
                int result = 0;
                try {
                    object = new JSONObject(responseData);
                    result = object.getInt("result");
                    System.out.println(object);
                    if (result == 1) {

                        objComment = object.getJSONObject("comment");
                        System.out.println(objComment);
                        Comment comment1 = parseJson(objComment.toString(), Comment.class);
                        HandlerPosition handlerPosition = new HandlerPosition();
                        handlerPosition.setObj(comment1);
                        handlerPosition.setPosition(position);
                        handler.sendMessage(handler.obtainMessage(0, handlerPosition));
                    } else {
                        handler.sendMessage(handler.obtainMessage(505));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }
}
