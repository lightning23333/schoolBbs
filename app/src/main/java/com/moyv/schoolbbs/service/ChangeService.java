package com.moyv.schoolbbs.service;

import android.os.Handler;
import android.util.Log;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static android.content.ContentValues.TAG;
import static com.moyv.schoolbbs.config.AccountConstant.ACCOUNT;
import static com.moyv.schoolbbs.config.AccountConstant.TOKEN;
import static com.moyv.schoolbbs.config.URLConstant.CHANGE_URL;
import static com.moyv.schoolbbs.util.HttpUtil.createClient;
import static com.moyv.schoolbbs.util.ToastUtils.show;

public class ChangeService {
    public void changeInfo(int index, String content, Handler handler) {
        String url = CHANGE_URL;
        String params = "";
        switch (index) {
            case 1:
//                用户名修改
                params = "name";
                break;
            case 2:
//                用户简介修改
                params = "introduction";
                break;
            case 3:
//                用户密码修改
                params = "password";
                break;
            default:
        }
        OkHttpClient okHttpClient = createClient();
        JSONObject obj = new JSONObject();
        try {
            obj.put("account", ACCOUNT);
            obj.put("params", params);
            obj.put("value", content);
        } catch (JSONException e) {
            Log.d(TAG, "getPost: json添加失败");
        }
        FormBody formBody = new FormBody.Builder()
                .add("request", obj.toString())
                .add("token", TOKEN)
                .build();
        Request request = new Request.Builder()
                .post(formBody)
                .url(url)
                .build();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                show("服务器连接失败");
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                String responseData = response.body().string();
                int result = 0;
                try {
                    JSONObject obj = new JSONObject(responseData);
                    result = obj.getInt("result");
                } catch (JSONException e) {
                    show("修改信息返回数据时解析失败");
                }
                switch (result) {
                    case 0:
                        show("非法的token");
                        break;
                    case 1:
                        show("信息修改成功");
                        handler.sendMessage(handler.obtainMessage(200));
                        break;
                    case 2:
                        show("信息修改失败");
                        break;
                    default:
                }


            }
        });
    }
}
