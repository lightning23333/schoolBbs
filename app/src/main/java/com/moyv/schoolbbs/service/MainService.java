package com.moyv.schoolbbs.service;

import android.os.Handler;
import android.util.Log;

import com.google.gson.Gson;
import com.moyv.schoolbbs.bean.UserInfo;
import com.moyv.schoolbbs.bean.ReModel;
import com.moyv.schoolbbs.pojo.Comment;
import com.moyv.schoolbbs.util.JsonUtil;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;


import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static android.content.ContentValues.TAG;
import static com.moyv.schoolbbs.config.URLConstant.MAIN_URL;
import static com.moyv.schoolbbs.config.URLConstant.USERINFO_URL;
import static com.moyv.schoolbbs.util.HttpUtil.createClient;
import static com.moyv.schoolbbs.util.JsonUtil.parseJson;
import static com.moyv.schoolbbs.util.JsonUtil.parseReModel;

/**
 * @author moyv
 */
public class MainService {
    public static int USER_POSTS = 123;
    public static int STAR_POSTS = 321;
    private String url;
    private ReModel reModel = null;
    private Handler handler = null;

    public MainService() {
    }

    public MainService(Handler handler) {
        this.handler = handler;
    }

    /**
     * 获取指定范围的帖子
     * @param star
     * @param end
     */

    public void getPosts(int star, int end) {
        url = MAIN_URL;
        System.out.println(url);
        OkHttpClient okHttpClient = createClient();
//        请求体
        JSONObject obj = new JSONObject();
        try {
            obj.put("star", star);
            obj.put("end", end);
            Log.e(TAG, "getPost: " + star);
            Log.e(TAG, "getPost: " + end);
        } catch (JSONException e) {
            Log.d(TAG, "getPost: json添加失败");
        }

        FormBody formBody = new FormBody.Builder()
                .add("result", obj.toString())
                .build();
//        发送请求操作
        Request request = new Request.Builder()
                .post(formBody)
                .url(url)
                .build();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                System.out.println("mainService.getPost:获取main的响应对象失败");
//                发送404到主页
                handler.sendMessage(handler.obtainMessage(404));
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                System.out.println("mainService.getPost:拿到main的响应对象");
                String responseData = response.body().string();
                reModel = parseReModel(responseData);
//                reModel = parseJson(responseData,ReModel.class);
                if (reModel == null) {
                    handler.sendMessage(handler.obtainMessage(404));
                } else {
                    handler.sendMessage(handler.obtainMessage(0, reModel));
                }

            }
        });
    }

    /**
     * 获取某个用户发表过的文章，用于文章管理
     * @param account
     * @param star
     * @param end
     * @param type
     */
    public void getUserPosts(int account, int star, int end, int type) {
        url = MAIN_URL;
        OkHttpClient okHttpClient = createClient();
//        请求体
        JSONObject obj = new JSONObject();
        try {
            obj.put("star", star);
            obj.put("end", end);
        } catch (JSONException e) {
            Log.d(TAG, "getPost: json添加失败");
        }

        FormBody formBody = new FormBody.Builder()
                .add("account", String.valueOf(account))
                .add("result", obj.toString())
                .add("type", String.valueOf(type))
                .build();
//        发送请求操作
        Request request = new Request.Builder()
                .post(formBody)
                .url(url)
                .build();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                System.out.println("mainService.getPost:获取main的响应对象失败");
//                发送404到主页
                handler.sendMessage(handler.obtainMessage(404));
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                System.out.println("mainService.getPost:拿到main的响应对象");
                String responseData = response.body().string();
                reModel = parseReModel(responseData);
                handler.sendMessage(handler.obtainMessage(0, reModel));
            }
        });
    }
}
