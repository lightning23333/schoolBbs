package com.moyv.schoolbbs.service;

import android.os.Handler;
import android.util.Log;

import com.moyv.schoolbbs.bean.LoginBean;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static android.content.ContentValues.TAG;
import static com.moyv.schoolbbs.config.URLConstant.LOGIN_URL;
import static com.moyv.schoolbbs.util.HttpUtil.createClient;
import static com.moyv.schoolbbs.util.JsonUtil.parseLoginBean;

/**
 * @author moyv
 */
public class LoginService {

    public void getLogin(int account, String password, Handler handler) {
        String url = LOGIN_URL;
        OkHttpClient okHttpClient = createClient();
        JSONObject obj = new JSONObject();
        try {
            obj.put("account", account);
            obj.put("password", password);
        } catch (JSONException e) {
            Log.d(TAG, "getPost: json添加失败");
        }
        FormBody formBody = new FormBody.Builder()
                .add("request", obj.toString())
                .build();
        Request request = new Request.Builder()
                .post(formBody)
                .url(url)
                .build();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                System.out.println("mainService.getPost:连接服务器失败");
                handler.sendMessage(handler.obtainMessage(404));
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                System.out.println("mainService.getPost:连接服务器成功");
                String responseData = response.body().string();
                System.out.println(responseData);
                int status = 0;
                try {
                    JSONObject jsonObject = new JSONObject(responseData);
                    status = jsonObject.getInt("status");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                switch (status) {
                    case 0:
                        System.out.println("用户不存在，请先注册");
                        handler.sendMessage(handler.obtainMessage(300));
                        break;
                    case 1:
                        System.out.println("用户登录成功");
                        LoginBean loginBean = parseLoginBean(responseData);
                        System.out.println("数据解析完毕");
                        if (loginBean == null) {
                            handler.sendMessage(handler.obtainMessage(500));
                        } else {
                            handler.sendMessage(handler.obtainMessage(200, loginBean));
                        }
                        break;
                    case 2:
                        System.out.println("密码错误");
                        System.out.println("密码错误");
                        handler.sendMessage(handler.obtainMessage(100));
                        break;
                    default:
                        System.out.println("status的值出错");
                }
            }
        });
    }
}
