package com.moyv.schoolbbs.service;

import android.os.Handler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.moyv.schoolbbs.config.URLConstant.UPLOAD_URL;
import static com.moyv.schoolbbs.util.HttpUtil.createClient;

public class PictureService {
    public static int PIC_POST = 202;
    public static int PIC_HEAD = 303;

    private static final MediaType MEDIA_TYPE_JPG = MediaType.parse("image/jpg");

    //图片上传
    public static void uploading(List<File> files, List<String> fileName, int postId, Handler handler) {
//图片上传接口地址
        String url = UPLOAD_URL;
        MultipartBody multipartBody = null;
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        builder.addFormDataPart("type", String.valueOf(PIC_POST));
        builder.addFormDataPart("postId", String.valueOf(postId));
        for (int i = 0; i < fileName.size(); i++) {
            File f = files.get(i);
            if (f != null) {
                builder.addFormDataPart("img", fileName.get(i), RequestBody.create(MEDIA_TYPE_JPG, f));
            }
        }

        multipartBody = builder.build();

//创建Request
        Request request = new Request.Builder()
                .url(url)
                .post(multipartBody)
                .build();
//创建okhttp对象
        OkHttpClient okHttpClient = createClient();
//上传完图片,得到服务器反馈数据
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                handler.sendMessage(handler.obtainMessage(404));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                int result = 404;
                try {
                    JSONObject obj = new JSONObject(response.body().string());
                    result = obj.getInt("result");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                switch (result) {
                    case 404:
                        handler.sendMessage(handler.obtainMessage(404));
                        break;
                    case 301:
                        handler.sendMessage(handler.obtainMessage(301));
                    case 200:
                        handler.sendMessage(handler.obtainMessage(200));
                    default:
                }
            }
        });
    }

    public static void uploadHead(String photo, int fileId, Handler handler) {
//图片上传接口地址
        String url = UPLOAD_URL;
//创建上传文件对象
        File file = new File(photo);
//创建RequestBody封装参数
        RequestBody fileBody = RequestBody.create(MEDIA_TYPE_JPG, file);
//创建MultipartBody,给RequestBody进行设置
        MultipartBody multipartBody = null;
        multipartBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("type", String.valueOf(PIC_HEAD))
                .addFormDataPart("postId", String.valueOf(fileId))
                .addFormDataPart("image", fileId + ".jpg", fileBody)
                .build();
//创建Request
        Request request = new Request.Builder()
                .url(url)
                .post(multipartBody)
                .build();
//创建okhttp对象
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .build();
//上传完图片,得到服务器反馈数据
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                handler.sendMessage(handler.obtainMessage(404));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                int result = 404;
                try {
                    JSONObject obj = new JSONObject(response.body().string());
                    result = obj.getInt("result");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                switch (result) {
                    case 404:
                        handler.sendMessage(handler.obtainMessage(404));
                        break;
                    case 301:
                        handler.sendMessage(handler.obtainMessage(301));
                    case 200:
                        handler.sendMessage(handler.obtainMessage(200));
                    default:
                }
            }
        });
    }
}
