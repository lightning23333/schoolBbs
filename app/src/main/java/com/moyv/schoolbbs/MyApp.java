package com.moyv.schoolbbs;

import android.app.Application;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;

import androidx.annotation.NonNull;

import java.util.logging.LogRecord;


/**
 * @author moyv
 */
public class MyApp extends Application {
    /**
     * 全局Context
     */

    private static Context context;


    private static int mScreenWidth;
    private static int mScreenHeight;
    private static float mDensity;

    private static Handler mHandler;
    @Override
    public void onCreate() {
        super.onCreate();
        initBase();
    }

    /**
     * 初始化
     */

    private void initBase() {
        initContext();
        initScreen();
    }



    private void initScreen() {
//        获取屏幕大小
//        高 mScreenHeight
//        宽 mScreenWidth
//        密度 density
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        mScreenWidth = dm.widthPixels;
        mScreenHeight = dm.heightPixels;
        mDensity = dm.density;
    }

    /**
     * 初始化全局变量
     */

    private void initContext() {
        context = getApplicationContext();
    }

    public static Context getContext() {
        return context;
    }
}
