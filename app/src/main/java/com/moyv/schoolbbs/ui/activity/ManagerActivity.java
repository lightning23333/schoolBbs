package com.moyv.schoolbbs.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.moyv.schoolbbs.R;
import com.moyv.schoolbbs.bean.ReModel;
import com.moyv.schoolbbs.pojo.Item;
import com.moyv.schoolbbs.service.MainService;
import com.moyv.schoolbbs.ui.adapter.DataAdapter;
import com.moyv.schoolbbs.util.LoadingUtil;
import com.moyv.schoolbbs.ui.listener.EndlessRecyclerOnScrollListener;

import java.util.ArrayList;
import java.util.List;

import static com.moyv.schoolbbs.base.Global.init;
import static com.moyv.schoolbbs.config.AccountConstant.ACCOUNT;
import static com.moyv.schoolbbs.config.URLConstant.IMAGE_URL;
import static com.moyv.schoolbbs.service.MainService.USER_POSTS;
import static com.moyv.schoolbbs.ui.adapter.DataAdapter.POSTMANAGER;
import static com.moyv.schoolbbs.ui.adapter.DataAdapter.STARAMNAGER;

/**
 * @author moyv
 * @描述 管理界面
 */
public class ManagerActivity extends AppCompatActivity {

    private static final int SUCCESS = 0;
    private static final int FALL = 404;
    private DataAdapter adapter;


    private Context mContext;
    private int start, end;
    private List<ReModel.Main> lists;
    //    帖子总数
    private int count;
    private int type;

    private RecyclerView recyclerView;
    private FloatingActionButton btnFlt;
    private SwipeRefreshLayout swiperefreshlayout;
    private LinearLayout llComment;
    private EditText etComment1;
    private TabLayout tabLayout;
    private TextView tvSend;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_manager);
        start = 0;
        end = 5;
        mContext = this;

        Intent intent = getIntent();
        type = intent.getIntExtra("type", USER_POSTS);
        recyclerView = findViewById(R.id.manager_recycler_view);
        llComment = findViewById(R.id.ll_comment);
        etComment1 = findViewById(R.id.et_comment);
        tabLayout = findViewById(R.id.bottom_tab_layout);
        tvSend = findViewById(R.id.tv_send_comment);


        init(mContext);

        swiperefreshlayout = findViewById(R.id.swiperefreshlayout);

        swiperefreshlayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                                                    @Override
                                                    public void onRefresh() {
                                                        Log.d("zttjiangqq", "invoke onRefresh...");
                                                        start = 0;
                                                        end = 5;
                                                        new MainService(handler1).getUserPosts(ACCOUNT, start, end, type);
                                                    }
                                                }

        );
        //      设置滑动监听器
        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener() {
            @Override
            public void onLoadMore() {
                adapter.setLoadState(adapter.LOADING);
                if (end < count) {
                    if (end + 5 > count) {
                        start = end + 1;
                        end = count;
                    } else {
                        start = end + 1;
                        end = start + 4;
                    }

                    new MainService(handler2).getPosts(start, end);
                } else {
                    // 显示加载到底的提示
                    adapter.setLoadState(adapter.LOADING_END);
                }


            }
        });
        new MainService(handler).getUserPosts(ACCOUNT, start, end, type);
//        加载帖子数据
        LoadingUtil.showDialogForLoading(mContext, "初始化数据中");
    }

    //    数据初始化
    Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            LoadingUtil.closeDialog(mContext);
            List<Item> items = new ArrayList<>();
            switch (msg.what) {
                case SUCCESS:
                    Toast.makeText(mContext, "成功获取数据", Toast.LENGTH_SHORT).show();
                    ReModel reModel = (ReModel) msg.obj;
                    lists = reModel.getMain();
                    count = reModel.getCount();
                    System.out.println(count);

                    for (ReModel.Main list : lists) {
                        Item item = new Item();
                        item.setMain(list);
                        System.out.println(list.getUserInfo().getIntroduction());
                        List<String> urls = new ArrayList<>();
                        for (int i = 0; i < list.getPost().getPostType(); i++) {
                            String url = IMAGE_URL + "/" + list.getPost().getPostId() + "/" + list.getPost().getPostId() + "_" + i + ".jpg";
                            urls.add(url);
                        }
                        item.setImagesUrls(urls);
                        items.add(item);
                    }

                    break;
                case FALL:
                    Toast.makeText(mContext, "连接服务器失败", Toast.LENGTH_SHORT).show();
                    break;
                default:
                    System.out.println("返回信息出错");
            }
            // 显示帖子列表
            recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
            adapter = new DataAdapter(mContext, items, llComment, etComment1, tvSend);
            adapter.type = type == USER_POSTS ? POSTMANAGER : STARAMNAGER;
            Log.i("TAG", "handleMessage: " + items.size());
            recyclerView.setAdapter(adapter);
            return false;
        }
    });

    //    数据刷新
    Handler handler1 = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(@NonNull Message msg) {
            swiperefreshlayout.setRefreshing(false);
            switch (msg.what) {
                case SUCCESS:
                    Toast.makeText(mContext, "成功获取数据", Toast.LENGTH_SHORT).show();
                    ReModel reModel = (ReModel) msg.obj;
                    lists = reModel.getMain();
                    count = reModel.getCount();
                    List<Item> items = new ArrayList<>();
                    for (ReModel.Main list : lists) {
                        Item item = new Item();
                        item.setMain(list);
                        List<String> urls = new ArrayList<>();
                        for (int i = 0; i < list.getPost().getPostType(); i++) {
                            String url = IMAGE_URL + "/" + list.getPost().getPostId() + "/" + list.getPost().getPostId() + "_" + i + ".jpg";
                            urls.add(url);
                        }
                        item.setImagesUrls(urls);
                        items.add(item);
                    }
                    adapter.refreshView(items);
                    break;
                case FALL:
                    Toast.makeText(mContext, "刷新失败", Toast.LENGTH_SHORT).show();
                    break;
                default:
                    break;
            }
            return false;
        }
    });

    //   数据添加
    Handler handler2 = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(@NonNull Message msg) {
            switch (msg.what) {
                case SUCCESS:
                    ReModel reModel = (ReModel) msg.obj;
                    List<ReModel.Main> addLists = reModel.getMain();
                    List<Item> items = new ArrayList<>();
                    for (ReModel.Main list : addLists) {
                        Item item = new Item();
                        item.setMain(list);
                        List<String> urls = new ArrayList<>();
                        for (int i = 0; i < list.getPost().getPostType(); i++) {
                            String url = IMAGE_URL + "/" + list.getPost().getPostId() + "/" + list.getPost().getPostId() + "_" + i + ".jpg";
                            urls.add(url);
                        }
                        item.setImagesUrls(urls);
                        items.add(item);
                    }
                    adapter.setLoadState(adapter.LOADING_COMPLETE);
                    adapter.addDatas(items);
                    break;
                case FALL:
                    Toast.makeText(mContext, "连接服务器失败", Toast.LENGTH_SHORT).show();
                    break;
                default:
                    break;
            }
            return false;
        }
    });
}
