package com.moyv.schoolbbs.ui.activity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.moyv.schoolbbs.R;
import com.moyv.schoolbbs.base.ActivityCollector;
import com.moyv.schoolbbs.ui.adapter.LaunchSimpleAdapter;

public class GuideActivity extends AppCompatActivity {

    private int[] lanuchImageArray = {
            R.drawable.guide_1, R.drawable.guide_2, R.drawable.guide_3, R.drawable.guide_4};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide);
        ActivityCollector.addActivity(this);

        ViewPager vp_launch = (ViewPager) findViewById(R.id.vp_launch);
        LaunchSimpleAdapter adapter = new LaunchSimpleAdapter(this, lanuchImageArray);
        vp_launch.setAdapter(adapter);
        vp_launch.setCurrentItem(0);
    }
}