package com.moyv.schoolbbs.ui.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.IBinder;

import androidx.annotation.Nullable;

import com.moyv.schoolbbs.base.chat.server.ConnectionServer;
import com.moyv.schoolbbs.base.chat.server.MessagesServer;
import com.moyv.schoolbbs.bean.model.User;
import com.moyv.schoolbbs.util.ToastUtils;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smackx.search.ReportedData;
import org.jivesoftware.smackx.search.UserSearchManager;
import org.jivesoftware.smackx.xdata.Form;
import org.jxmpp.jid.impl.JidCreate;
import org.jxmpp.stringprep.XmppStringprepException;

import java.util.List;

public class ChatService extends Service {
    private ConnectionServer connectionServer;
    private MessagesServer messagesServer;

    private OnProgressListener onProgressListener;//用于交互的回调接口

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ConnectionServer getConnectionServer() {
        return connectionServer;
    }

    public MessagesServer getMessagesServer() {
        return messagesServer;
    }

//    public MessagesServer.Conversation openConversation(String id,OnConversationListener onConversationListener) {
//        return messagesServer.createConversation(onConversationListener,id);
//    }

    /**
     * 注册回调接口的方法，供外部调用
     *
     * @param onProgressListener
     */
    public void setOnProgressListener(OnProgressListener onProgressListener) {
        this.onProgressListener = onProgressListener;
    }


    public class MBinder extends Binder {
        /**
         * 获取当前Service的实例
         *
         * @return
         */
        public ChatService getService() {
            return ChatService.this;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
//        initService();
        return new MBinder();
    }

    //连接服务 自动登录
    public boolean initService() {
        connectionServer = new ConnectionServer();
        messagesServer = new MessagesServer();
        if (autoLogin()) {
            System.out.println("自动登陆成功！");
            return true;
        } else {
            System.out.println("自动登陆失败！！！");
            return false;
        }
    }

    //自动登录
    public boolean autoLogin() {
        SharedPreferences sharedPreferences = getSharedPreferences("login", Context.MODE_PRIVATE);
        System.out.println("--------isLogin:" + sharedPreferences.getBoolean("isLogin", false));
        System.out.println("--------Account:" + sharedPreferences.getInt("account", 0));
        System.out.println("--------Password:" + sharedPreferences.getString("password", null));
        if (sharedPreferences.getBoolean("isLogin", false)) {
            if (login(String.valueOf(sharedPreferences.getInt("account", 0)), sharedPreferences.getString("password", null)))
                return true;
        }
        return false;
    }

    //切换账号
    public boolean checkOutLogin(String name, String pass) {
        if (connectionServer.isLogin())
            connectionServer.logout();
        if (login(name, pass))
            return true;
        return false;
    }

    //登录
    public boolean login(String name, String pass) {
        if (name.equals("0") || pass == null)
            return false;
        if (connectionServer.isLogin())
            return false;
        if (connectionServer.login(name, pass)) {
            messagesServer.init(connectionServer.getConnection(), onProgressListener);
            id = name;
        }
        return true;
    }

    //登出
    public boolean logout() {
        if (connectionServer != null) {
            return connectionServer.logout();
        }
        return false;
    }

    //注册
    public boolean register(String account, String name, String pass) {
        if (connectionServer.isConnected())
            return connectionServer.register(account, name, pass);
        return false;
    }

    //搜索
    public User searchFirst(String account) {
        return connectionServer.searchFirst(account);
    }

    public List<User> search(String account) {
        return connectionServer.search(account);
    }

}
