package com.moyv.schoolbbs.ui.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;


import com.moyv.schoolbbs.R;
import com.moyv.schoolbbs.base.ActivityCollector;

/**
 * 欢迎界面
 */

public class WelcomeActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        ActivityCollector.addActivity(this);
    }



    @Override
    protected void onStart() {
        super.onStart();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                /*
                 实现第一次使用App跳转到指南界面
                 采用SharedPreferences键值对形式来判定是否是第一次启动
                 1.用base文件中isFirstStart的键值对存储状态，true代表是第一次启动，false代表不是
                 2.如果是第一次启动，修改isFirstStart状态，保证下一次打开App直接跳主页，最后跳转到指南界面
                 */
                /*
                SharedPreferences的读取数据步骤
                1.创建一个SharedPreferences对象
                2.通过键进行数据读取
                 */
                /*
                SharedPreferences存储数据的步骤
                1.创建一个SharedPreferences对象
                2.实例化一个SharedPreferences.Editor对象
                3.通过Editor保存存储数据
                4.Editor提交
                 */
                SharedPreferences base = getSharedPreferences("base",MODE_PRIVATE);
                boolean isFirstStart = base.getBoolean("isFirstStart",true);

                Intent intent = null;
                if(isFirstStart) {
                    //代表第一次启动App
                    intent = new Intent(WelcomeActivity.this, GuideActivity.class);
                    SharedPreferences.Editor editor = base.edit();
                    editor.putBoolean("isFirstStart",false);
                    editor.commit();
                } else {
                    intent = new Intent(WelcomeActivity.this, BottomTabLayoutActivity.class);
                }

                startActivity(intent);
                finish();
            }
        }, 3000);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ActivityCollector.removeActivity(this);
    }
}
