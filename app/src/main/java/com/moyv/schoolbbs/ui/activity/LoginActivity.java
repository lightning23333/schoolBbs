package com.moyv.schoolbbs.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.moyv.schoolbbs.R;
import com.moyv.schoolbbs.bean.LoginBean;
import com.moyv.schoolbbs.service.LoginService;

import static android.content.ContentValues.TAG;

/**
 * @author moyv
 */
public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private Context mContext;
    private String password;

    private EditText etName;
    private EditText etPsw;
    private Button btnConfirm;
    private CheckBox cbRm;
    private TextView register;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();
    }

    /**
     * 初始化
     */
    public void initView() {
        mContext = this;
        etName = findViewById(R.id.et_username);
        etPsw = findViewById(R.id.et_password);
        btnConfirm = findViewById(R.id.btn_confirm);
        cbRm = findViewById(R.id.cb_rm);
        register = findViewById(R.id.login_btn_register);

        register.setOnClickListener(this);
        btnConfirm.setOnClickListener(this);
        SharedPreferences sharedPreferences = getSharedPreferences("login", 0);
        int account = sharedPreferences.getInt("account", 0);
        Log.e(TAG, "initView: " + account);
        etName.setText(account == 0 ? "" : String.valueOf(account));
        if (sharedPreferences.getBoolean("rememberPsw", false)) {
            cbRm.setChecked(true);
            etPsw.setText(sharedPreferences.getString("password", ""));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_confirm:
                send();
                break;
            case R.id.login_btn_register:
                Intent intent = new Intent(mContext, RegisterActivity.class);
                startActivity(intent);
                finish();
                break;
            default:
                System.out.println("点击事件没有注册事件");
        }
    }

    //    登录方法
    public void send() {
        String userName = etName.getText().toString();
        password = etPsw.getText().toString();
        LoginService loginService = new LoginService();
        Handler handler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(@NonNull Message msg) {
                switch (msg.what) {
                    case 200:
                        LoginBean loginBean = (LoginBean) msg.obj;
                        Log.d(TAG, "onResponse: " + loginBean.getAccount());
                        Log.d(TAG, "onResponse: " + loginBean.getName());
                        Log.d(TAG, "onResponse: " + loginBean.getStatus());
                        Log.d(TAG, "onResponse: " + loginBean.getIntroduction());
                        Log.d(TAG, "onResponse: " + loginBean.getToken());
                        save(loginBean);
                        Toast.makeText(mContext, "登录成功", Toast.LENGTH_SHORT).show();
                        finish();
                        break;
                    case 404:
                        Toast.makeText(mContext, "没找到服务器", Toast.LENGTH_SHORT).show();
                        break;
                    case 500:
                        Toast.makeText(mContext, "返回错误数据", Toast.LENGTH_SHORT).show();
                        break;
                    case 300:
                        etName.setText("");
                        etPsw.setText("");
                        Toast.makeText(mContext, "用户不存在，请先注册", Toast.LENGTH_SHORT).show();
                        break;
                    case 100:
                        etPsw.setText("");
                        Toast.makeText(mContext, "密码错误", Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        System.out.println("msg返回错误的编号");
                }
                return false;
            }
        });
        loginService.getLogin(Integer.parseInt(userName), password, handler);
    }

    /**
     * 登录成功后保存token和账号信息
     */
    public void save(LoginBean loginBean) {
        SharedPreferences sharedPreferences = getSharedPreferences("login", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("account", loginBean.getAccount());
        editor.putString("name", loginBean.getName());
        editor.putString("introduction", loginBean.getIntroduction());
        editor.putString("token", loginBean.getToken());
        editor.putBoolean("isLogin", true);
        if (cbRm.isChecked()) {
            editor.putBoolean("rememberPsw", true);
//            editor.putString("password",password);
        } else {
            editor.putBoolean("rememberPsw", false);
        }
        editor.putString("password", password);
        editor.commit();
//        new AccountConstant(context);
    }
}
