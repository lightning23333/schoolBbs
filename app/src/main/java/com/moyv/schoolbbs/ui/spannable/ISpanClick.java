package com.moyv.schoolbbs.ui.spannable;


/**
 * @author moyv
 */
public interface ISpanClick {
    public void onClick(int position);
}
