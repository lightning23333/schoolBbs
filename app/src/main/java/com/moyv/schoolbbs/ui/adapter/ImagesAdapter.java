package com.moyv.schoolbbs.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.moyv.schoolbbs.R;
import com.moyv.schoolbbs.base.Global;
import com.moyv.schoolbbs.ui.activity.PlusImageActivity;
import com.moyv.schoolbbs.ui.config.MainConstant;

import java.util.ArrayList;
import java.util.List;

import static android.widget.ImageView.ScaleType.FIT_START;
import static com.moyv.schoolbbs.ui.config.MainConstant.MAIN_SHOW;
import static com.moyv.schoolbbs.ui.config.MainConstant.SHOW;


/**
 * @author moyv
 * @描述 网格图片适配器
 */
public class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.ViewHolder> {
    public Context context;
    public List<String> urls;
    private ImageView imageView;

    public ImagesAdapter(Context context, List<String> urls) {
        this.context = context;
        this.urls = urls;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_fragment_main_image, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        imageView = holder.imageView;
        Glide.with(context)
                .load(urls.get(position))
                .placeholder(R.drawable.ic_picture_loader_fail)
                .fallback(R.drawable.ic_picture_loader_fail)
                .centerCrop()
                .into(imageView);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PlusImageActivity.class);
                intent.putExtra(SHOW, MAIN_SHOW);
                intent.putStringArrayListExtra(MainConstant.IMG_LIST, (ArrayList<String>) urls);
                intent.putExtra(MainConstant.POSITION, position);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return urls.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.iv_image);
            ViewGroup.LayoutParams params = itemView.getLayoutParams();
            if (urls.size() != 1) {
                params.height = Global.getGridWidth()-25;
                params.width = Global.getGridWidth()-25;
            } else {
                imageView.setScaleType(FIT_START);
                params.width = Global.getGridWidth() * 2;
                params.height = Global.getGridWidth() * 2;

            }
        }
    }
}
