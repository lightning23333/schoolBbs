package com.moyv.schoolbbs.ui.fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.moyv.schoolbbs.R;
import com.moyv.schoolbbs.base.chat.data.DialogData;
import com.moyv.schoolbbs.base.chat.data.SealData;
import com.moyv.schoolbbs.bean.model.Dialog;
import com.moyv.schoolbbs.bean.model.Message;
import com.moyv.schoolbbs.bean.model.User;
import com.moyv.schoolbbs.config.AccountConstant;
import com.moyv.schoolbbs.ui.activity.BottomTabLayoutActivity;
import com.moyv.schoolbbs.ui.activity.ChatActivity;
import com.moyv.schoolbbs.ui.service.ChatService;
import com.moyv.schoolbbs.util.ToastUtils;
import com.stfalcon.chatkit.dialogs.DialogsList;
import com.stfalcon.chatkit.dialogs.DialogsListAdapter;
import com.xuexiang.xui.widget.dialog.materialdialog.MaterialDialog;

import java.util.Date;

public class MessageFragment extends Fragment {
    private Context mContext;
    private DialogsList dialogsList;
    private DialogsListAdapter<Dialog> dialogsListAdapter;
    private BottomTabLayoutActivity activity;

    private ServiceConnection conn;
    private ChatService chatService;

    private DialogData dialogData;
    private Dialog saveDialog;

    private Toolbar toolbar;


    public static MessageFragment newInstance(String title) {
        Bundle arguments = new Bundle();
        arguments.putString("TITLE", title);
        MessageFragment fragment = new MessageFragment();
        fragment.setArguments(arguments);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_message, container, false);
        mContext = getContext();
        activity = (BottomTabLayoutActivity) getActivity();
        dialogsList = root.findViewById(R.id.dialogsList);
        toolbar = root.findViewById(R.id.fragment_chat_toolbar);
        setToolBar();
        dialogData = new DialogData(mContext, String.valueOf(AccountConstant.ACCOUNT));
        initDialogList();

        //绑定Chat后台服务
        Intent intent = new Intent(mContext, ChatService.class);
        System.out.println("context,this" + this);
        initConn();
        mContext.bindService(intent, conn, Context.BIND_AUTO_CREATE);

        return root;
    }

    //构造列表
    private void initDialogList() {
        dialogsListAdapter = new DialogsListAdapter<>((imageView, url, payload) -> {
            Glide.with(mContext).load(url).into(imageView);
        });
        dialogsListAdapter.addItems(dialogData.getLocalRecord());//数据源
        dialogsListAdapter.setOnDialogClickListener(this::openChat);//聊天界面
        dialogsListAdapter.setOnDialogLongClickListener(dialog -> ToastUtils.show(dialog.getDialogName()));
        dialogsList.setAdapter(dialogsListAdapter);
    }

    @SuppressLint("NotifyDataSetChanged")
    private void openChat(Dialog dialog) {
        ChatActivity.open(mContext, chatService.getMessagesServer(), dialog, dialogData);
        saveDialog = dialog;
        dialog.setUnreadCount(0);
        dialogsListAdapter.notifyDataSetChanged();
        dialogData.updateDialogUnreadCount(dialog.getId(), 0);
    }

    //绑定service回调
    private void initConn() {
        conn = new ServiceConnection() {
            /**
             * 与服务器端交互的接口方法 绑定服务的时候被回调，在这个方法获取绑定Service传递过来的IBinder对象，
             * 通过这个IBinder对象，实现宿主和Service的交互。
             */
            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                ChatService.MBinder mBinder = (ChatService.MBinder) service;
                chatService = mBinder.getService();
                activity.setChatService(chatService);
                //消息监听回调接口
                chatService.setOnProgressListener((message) -> {
                    System.out.println("回调接受消息：" + message.getBody());
                    String n = message.getFrom().getLocalpartOrThrow().toString();
                    String mes = message.getBody();
                    String Name = chatService.searchFirst(n).getName();
                    dialogData.saveMessage(n, n, mes, true);
                    Dialog itemById = dialogsListAdapter.getItemById(n);
                    System.out.println("itemByid:" + itemById);
                    if (itemById == null) {
                        Dialog dialog = dialogData.CreateDialog(n, Name, mes);
                        activity.runOnUiThread(() -> {
                            dialogsListAdapter.addItem(0, dialog);
                        });
                    } else {
                        itemById.setLastMessage(SealData.getMessage(n, mes, new Date()));
                        itemById.setUnreadCount(itemById.getUnreadCount() + 1);
//                        dialogsListAdapter.
                        activity.runOnUiThread(() -> {
                            dialogsListAdapter.notifyDataSetChanged();
                        });
                        dialogData.updateDialogUnreadCount(n, itemById.getUnreadCount());
                    }
                });

//                实现异步自动登录
                new Thread(() -> {
                    if(chatService.initService()) {
                        loginHandler.sendMessage(loginHandler.obtainMessage(200));
                    }
                }).start();
            }

            /**
             * 当取消绑定的时候被回调。但正常情况下是不被调用的，它的调用时机是当Service服务被意外销毁时，
             * 例如内存的资源不足时这个方法才被自动调用。
             */
            @Override
            public void onServiceDisconnected(ComponentName name) {
                ToastUtils.showToast(mContext, "Chat后台服务被终结！！！", true);
                System.out.println("Chat后台服务被终结！！！");
                chatService = null;
            }
        };
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void onResume() {
        super.onResume();
        if (saveDialog != null) {
            saveDialog.setUnreadCount(0);
            Message message = dialogData.getLastMessage(saveDialog.getId());
            saveDialog.setLastMessage(message);
            dialogsListAdapter.notifyDataSetChanged();
            dialogData.updateDialogUnreadCount(saveDialog.getId(), 0);
            saveDialog = null;
        }
        System.out.println(dialogData.getName() + "," + AccountConstant.ACCOUNT);
        if (!dialogData.getName().equals(String.valueOf(AccountConstant.ACCOUNT))) {
            dialogData = new DialogData(mContext, String.valueOf(AccountConstant.ACCOUNT));
            dialogsListAdapter.clear();
            dialogsListAdapter.addItems(dialogData.getLocalRecord());
        }
        if (!AccountConstant.LOGIN) {
            dialogsListAdapter.clear();
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    /**
     * 设置标签栏的menu菜单
     */
    private void setToolBar() {
        toolbar.inflateMenu(R.menu.mymenu);
        toolbar.setOnMenuItemClickListener(item -> {
            onOptionsItemSelected(item);
            return true;
        });
    }



    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.add(0, 1, 0, "添加好友").setIcon(R.drawable.btn_white_add)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
//        //对整个菜单进行显示
//        menu.setGroupVisible(R.menu.mymenu, true);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Toast.makeText(mContext, item.getItemId(), Toast.LENGTH_SHORT).show();
        if (item.getItemId() == R.id.add) {
            if (!AccountConstant.LOGIN) {
                ToastUtils.showToast(mContext,"请先登录",false);
                return false;
            }
            final EditText et = new EditText(mContext);
            new MaterialDialog.Builder(mContext)
                    .customView(et, true)
                    .title("请输入账号：")
                    .positiveText("搜索")
                    .negativeText("取消")
                    .onPositive((dialog, which) -> {
                        User user = chatService.searchFirst(et.getText().toString());
                        if (user != null) {
                            if (user.getId().equals(AccountConstant.ACCOUNT+"")){
                                ToastUtils.showToast(mContext,"人格分裂患者请及时就医",false);
                                return;
                            }
                            new MaterialDialog.Builder(mContext)
                                    .title("找到用户：" + user.getName())
                                    .positiveText("连接")
                                    .negativeText("取消")
                                    .onPositive((dialog1, which1) -> {
                                        Dialog itemById = dialogsListAdapter.getItemById(user.getId());
                                        if (itemById == null) {
                                            Dialog createDialog = dialogData.CreateDialog(user.getId(), user.getName(), "");
                                            openChat(createDialog);
                                            dialogsListAdapter.addItem(0, createDialog);
                                        } else {
                                            openChat(itemById);
                                        }
                                    }).show();
                        } else {
                            ToastUtils.showToast(mContext, "未找到", false);
                        }
                    })
                    .show();
            return true;
        }
        return false;
    }

    //    登录处理
    Handler loginHandler = new Handler(msg -> {
        switch (msg.what) {
            case 200:
                ToastUtils.showToast(mContext, "自动登录成功", false);
                break;
            default:
        }
        return false;
    });
}
