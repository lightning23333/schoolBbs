package com.moyv.schoolbbs.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.moyv.schoolbbs.R;
import com.moyv.schoolbbs.ui.config.CancelOrOkDialog;
import com.moyv.schoolbbs.ui.adapter.ViewPagerAdapter;
import com.moyv.schoolbbs.ui.config.MainConstant;

import java.util.ArrayList;

import static com.moyv.schoolbbs.ui.config.MainConstant.MAIN_SHOW;
import static com.moyv.schoolbbs.ui.config.MainConstant.SHOW;

/**
 * @author moyv
 * @描述 发动态界面点击查看图片
 */
public class PlusImageActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener, View.OnClickListener {
    /**
     * 展示图片的ViewPager
     */
    private ViewPager viewPager;
    /**
     * 图片的位置，第几张图片
     */
    private TextView positionTv;
    private ImageView ivBack;
    private ImageView ivDelete;
    /**
     * 图片的数据源
     */
    private ArrayList<String> imgList;
    /**
     * 第几张图片
     */
    private int mPosition;
    private ViewPagerAdapter mAdapter;
    private String showType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        setContentView(R.layout.activity_plus_image);
        showType = getIntent().getStringExtra(SHOW);
        imgList = getIntent().getStringArrayListExtra(MainConstant.IMG_LIST);
        mPosition = getIntent().getIntExtra(MainConstant.POSITION, 0);
        initView();
    }

    private void initView() {
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        positionTv = (TextView) findViewById(R.id.position_tv);
        ivBack = findViewById(R.id.back_iv);
        ivDelete = findViewById(R.id.delete_iv);
        if (showType.equals(MAIN_SHOW)) {
            ivDelete.setVisibility(View.GONE);
        } else {
            ivDelete.setOnClickListener(this);
        }
        ivBack.setOnClickListener(this);
        viewPager.addOnPageChangeListener(this);

        mAdapter = new ViewPagerAdapter(this, imgList);
        viewPager.setAdapter(mAdapter);
        positionTv.setText(mPosition + 1 + "/" + imgList.size());
        viewPager.setCurrentItem(mPosition);
    }

    /**
     * 删除图片
     */

    private void deletePic() {
        CancelOrOkDialog dialog = new CancelOrOkDialog(this, "要删除这张图片吗?") {
            @Override
            public void ok() {
                super.ok();
                imgList.remove(mPosition); //从数据源移除删除的图片
                if (mPosition == 1) {
                    back();
                }
                setPosition();
                dismiss();
            }
        };
        dialog.show();
    }

    /**
     * 设置当前位置
     */

    private void setPosition() {
        positionTv.setText(mPosition + 1 + "/" + imgList.size());
        viewPager.setCurrentItem(mPosition);
        mAdapter.notifyDataSetChanged();
    }

    /**
     * 返回上一个页面
     */

    private void back() {
        Intent intent = getIntent();
        intent.putStringArrayListExtra(MainConstant.IMG_LIST, imgList);
        setResult(MainConstant.RESULT_CODE_VIEW_IMG, intent);
        finish();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        mPosition = position;
        positionTv.setText(position + 1 + "/" + imgList.size());
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_iv:
                //返回
                back();
                break;
            case R.id.delete_iv:
                //删除图片
                deletePic();
                break;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //按下了返回键
            back();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
