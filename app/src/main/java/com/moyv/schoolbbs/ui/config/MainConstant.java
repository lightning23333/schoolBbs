package com.moyv.schoolbbs.ui.config;

/**
 * @author moyv
 */

public class MainConstant {
    /**
     * 第几张图片
     * 第几张图片
     * 图片路径
     * 主页展示
     * 添加页面展示
     * 最多上传9张图片
     * 请求码
     * 查看大图页面的结果码
     */
    public static final String IMG_LIST = "img_list";
    public static final String POSITION = "position";
    public static final String PIC_PATH = "pic_path";
    public static final String SHOW = "show";
    public static final String MAIN_SHOW = "main_show";
    public static final String ADD_SHOW = "add_show";
    public static final int MAX_SELECT_PIC_NUM = 9;
    public static final int REQUEST_CODE_MAIN = 10;
    public static final int RESULT_CODE_VIEW_IMG = 11;
}
