package com.moyv.schoolbbs.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.moyv.schoolbbs.R;
import com.moyv.schoolbbs.ui.fragment.HomeFragment;
import com.moyv.schoolbbs.ui.fragment.MeFragment;
import com.moyv.schoolbbs.ui.fragment.MessageFragment;

/**
 * @author moyv
 * @描述 底部栏数据设置
 */
public class DataGenerator {

    public static final int[] mTabRes = new int[]{R.drawable.tab_home_selector2, R.drawable.tab_message_selector, R.drawable.tab_me_selector};
    public static final int[] mTabResPressed = new int[]{R.drawable.tab_home_selector3, R.drawable.tab_message_selector1, R.drawable.tab_me_selector1};
    public static final String[] mTabTitle = new String[]{"首页", "消息", "未登录"};

    public static Fragment[] getFragments(String from) {
        Fragment fragments[] = new Fragment[3];
        fragments[0] = HomeFragment.newInstance(from);
        fragments[1] = MessageFragment.newInstance(from);
        fragments[2] = MeFragment.newInstance(from);
        return fragments;
    }

    /**
     * 获取Tab 显示的内容
     *
     * @param context
     * @param position
     * @return
     */
    public static View getTabView(Context context, int position) {
        View view = LayoutInflater.from(context).inflate(R.layout.home_tab_content, null);
        ImageView tabIcon = view.findViewById(R.id.tab_content_image);
        tabIcon.setImageResource(DataGenerator.mTabRes[position]);
        TextView tabText = view.findViewById(R.id.tab_content_text);
        tabText.setText(mTabTitle[position]);
        return view;
    }
}