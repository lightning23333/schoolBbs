package com.moyv.schoolbbs.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.moyv.schoolbbs.R;
import com.moyv.schoolbbs.config.AccountConstant;
import com.moyv.schoolbbs.service.RegisterService;
import com.moyv.schoolbbs.ui.service.ChatService;

import static com.moyv.schoolbbs.util.ToastUtils.initToastUtil;
import static com.moyv.schoolbbs.util.ToastUtils.show;

/**
 * @author moyv
 * @描述 注册界面
 */

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    private Context mContext;
    private int account;

    private EditText etName;
    private EditText etAccount;
    private EditText etPassword;
    private EditText etAgainPsw;
    private Button btnRegister;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        mContext = this;
        intiView();
        checkLegal();
    }

    public void intiView() {
        initToastUtil(this);
        etName = findViewById(R.id.register_et_name);
        etAccount = findViewById(R.id.register_et_account);
        etPassword = findViewById(R.id.register_et_password);
        etAgainPsw = findViewById(R.id.register_et_againPsw);
        btnRegister = findViewById(R.id.register_btn_register);

        btnRegister.setOnClickListener(this);
    }

    public void checkLegal() {
//    验证输入合法性

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.register_btn_register:
                String name = etName.getText().toString();

                account = Integer.parseInt(etAccount.getText().toString());
                String password = etPassword.getText().toString();
                String pswAgain = etAgainPsw.getText().toString();
                if (!password.equals(pswAgain)) {
                    show("两次输入的密码不一样");
                } else {
                    RegisterService registerService = new RegisterService();
                    registerService.register(account, name, password, handler);
                    //注册chat
                    ChatService chatService = BottomTabLayoutActivity.getChatService();
                    if (chatService == null)
                        System.out.println("chatService == null !!!!!");
                    else {
                        if (chatService.register(String.valueOf(account),name,password))
                            System.out.println("chat账号注册成功");
                        else
                            System.out.println("chat账号注册失败");
                    }
                }
                break;
            default:
        }
    }

    Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(@NonNull Message msg) {
            switch (msg.what) {
                case 200:
                    show("注册成功");
                    SharedPreferences sharedPreferences = getSharedPreferences("login", 0);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putInt("account", account);
                    editor.putBoolean("isLogin", true);
                    new AccountConstant(mContext);
                    Intent intent = new Intent(mContext, LoginActivity.class);
                    startActivity(intent);
                    finish();
                    break;
                case 500:
                    show("注册失败");
                    break;
                case 301:
                    show("账号已存在");
                    break;
                case 302:
                    show("用户名已存在");
                    break;
                case 404:
                    show(getClass().getName() + "连接服务器失败");
                    break;
                default:
                    show(getClass().getName() + "非法result");
            }
            return false;
        }
    });
}
