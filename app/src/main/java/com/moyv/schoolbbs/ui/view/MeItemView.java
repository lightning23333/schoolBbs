package com.moyv.schoolbbs.ui.view;

/**
 * @author moyv
 */

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.moyv.schoolbbs.R;

/**
 * 自定义个人中心选项控件
 * @author moyv
 */
public class MeItemView extends RelativeLayout {
    private TextView data;

    public MeItemView(final Context context, AttributeSet attrs){
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.item_me_item_menu, this);
        @SuppressLint("CustomViewStyleable") TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.MeItemView);

        ImageView icon = findViewById(R.id.icon);
        ImageView more = findViewById(R.id.more);
        ImageView line = findViewById(R.id.line);
        TextView name = findViewById(R.id.name);
        data = findViewById(R.id.data);

        icon.setImageDrawable(typedArray.getDrawable(R.styleable.MeItemView_icon));
        name.setText(typedArray.getText(R.styleable.MeItemView_name));

        if (typedArray.getBoolean(R.styleable.MeItemView_show_more, false)){
            more.setVisibility(VISIBLE);
        }
        if (typedArray.getBoolean(R.styleable.MeItemView_show_line, false)){
            line.setVisibility(VISIBLE);
        }
        typedArray.recycle();
    }

    // 提供设置控件的描述数据
    public void setData(String data){
        this.data.setText(data);
    }
    public TextView getData(){
        return data;
    }
}

