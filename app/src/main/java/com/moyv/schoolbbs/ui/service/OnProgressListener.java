package com.moyv.schoolbbs.ui.service;

import org.jivesoftware.smack.packet.Message;

/*
* service将监听到的信息传递给UI
*
* */
public interface OnProgressListener {
    void onProgress(Message message);
}
