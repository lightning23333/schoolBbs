package com.moyv.schoolbbs.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.viewpager.widget.PagerAdapter;

import com.moyv.schoolbbs.R;
import com.moyv.schoolbbs.ui.activity.BottomTabLayoutActivity;

import java.util.ArrayList;

public class LaunchSimpleAdapter extends PagerAdapter {

    private LayoutInflater mInflater;
    private Context mContext;
    private ArrayList<View> mViewList = new ArrayList<View>();

    public LaunchSimpleAdapter(Context context, int[] imageArray) {
        mInflater = LayoutInflater.from(context);
        mContext = context;
        for (int i=0; i<imageArray.length; i++) {
            View view = mInflater.inflate(R.layout.item_guide, null);
            ImageView iv_launch = (ImageView) view.findViewById(R.id.item_guide_iv_lunch);
            RadioGroup rg_indicate = (RadioGroup) view.findViewById(R.id.item_guide_rg_indicate);
            Button btn_start = (Button) view.findViewById(R.id.item_guide_btn_start);
            iv_launch.setImageResource(imageArray[i]);
            for (int j=0; j<imageArray.length; j++) {
                RadioButton radio = new RadioButton(mContext);
                radio.setLayoutParams(new RadioGroup.LayoutParams(RadioGroup.LayoutParams.WRAP_CONTENT, RadioGroup.LayoutParams.WRAP_CONTENT));   //android.view.ViewGroup.LayoutParams
                radio.setButtonDrawable(R.drawable.launch_guide_radio_btn);
                radio.setPadding(10, 10, 10, 10);
                rg_indicate.addView(radio);
            }
            ((RadioButton)rg_indicate.getChildAt(i)).setChecked(true);
            if (i == imageArray.length-1) {
                btn_start.setVisibility(View.VISIBLE);
                btn_start.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, BottomTabLayoutActivity.class);
                        context.startActivity(intent);
                        Toast.makeText(mContext, "Welcome to XXX App", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            mViewList.add(view);
        }
    }

    @Override
    public int getCount() {
        return mViewList.size();
    }

    @Override
    public boolean isViewFromObject(View arg0, Object arg1) {
        return arg0 == arg1;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView(mViewList.get(position));
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        container.addView(mViewList.get(position));
        return mViewList.get(position);
    }

}