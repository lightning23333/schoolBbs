package com.moyv.schoolbbs.config;

/**
 * @author moyv
 */
public class URLConstant {
//    tomcat位置
//    public static final String HOST="http://192.168.0.101:8080/bbs_servlet";
//    服务器位置
    public static final String HOST="http://www.lyczs.xyz:8080/bbs_servlet";
    public static final String MAIN_URL=HOST+"/main";
    public static final String LOGIN_URL=HOST+"/login";
    public static final String REGISTER_URL=HOST+"/register";
    public static final String USERINFO_URL=HOST+"/userInfo";
    public static final String RELPOST_URL=HOST+"/relPost";
    public static final String UPLOAD_URL=HOST+"/upload";
    public static final String IMAGE_URL=HOST+"/images";
    public static final String HEAD_URL=HOST+"/images/head/";
    public static final String CMT_URL=HOST+"/cmt";
    public static final String STAR_URL=HOST+"/star";
    public static final String CHANGE_URL=HOST+"/change";
    public static final String DELETE_URL=HOST+"/deletePost";

    //ChatServiceConfig
    public static final String C_HOST="47.111.141.140";
    public static final int C_PORT=5222;
    public static final String C_DOMAIN="up55";
    public static final String C_SOURCE="123456";
}
