package com.moyv.schoolbbs.config;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * @author moyv
 * @描述 sharedPreferences文件的数据作为全局变量
 */
public class AccountConstant {
    public static boolean LOGIN;
    public static int ACCOUNT;
    public static String TOKEN;
    public static String NAME;
    public static String INTRODUCTION;

    public AccountConstant(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("login", 0);
        LOGIN = sharedPreferences.getBoolean("isLogin", false);
        if (LOGIN) {
            ACCOUNT = sharedPreferences.getInt("account", 0);
            TOKEN = sharedPreferences.getString("token", "");
            NAME = sharedPreferences.getString("name", "");
            INTRODUCTION = sharedPreferences.getString("introduction", "");
        }
    }
}
